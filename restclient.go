package restclient

import (
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// RESTClient shared data between functions
type RESTClient struct {
	HTTPClient http.Client
	IP         string
	Port       string
	JWTToken   string
	Secret     []byte
}

// RequestSysMonitor returns all monitored values from REST server
func (R *RESTClient) RequestSysMonitor() (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "sysmonitor"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return body, nil
}

// RequestLoginHandlers returns all login handlers from REST server
func (R *RESTClient) RequestLoginHandlers() (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "loginhandlers"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return body, nil
}

// RequestConnectionHandlers returns all connection handlers from REST server
func (R *RESTClient) RequestConnectionHandlers() (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "connectionhandlers"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return body, nil
}

// RequestLeastCrowdedConnectionHandler returns all connection handlers from REST server
func (R *RESTClient) RequestLeastCrowdedConnectionHandler() (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "leastcrowdedconnectionhandler"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return body, nil
}

// RequestCheckLoginToken checks given login token against one in REST server.
func (R *RESTClient) RequestCheckLoginToken(username, token string) (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "checklogintoken"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("Name", username)
	req.Header.Add("Token", token)

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return body, nil
}

// RequestWhatsMyIP returns the client IP in HTTP request header
func (R *RESTClient) RequestWhatsMyIP() (string, error) {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "whatsmyip"

	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return "", err
	}

	body, err := R.sendRequest(req)
	if err != nil {
		return "", err
	}

	return strings.Split(body, ":")[0], nil
}

// InsertLoginHandler inserts a single login handler to rest server that expires
// in 10 seconds.
func (R *RESTClient) InsertLoginHandler(name, ip, port string) error {

	connString := "https://" + R.IP + ":" + R.Port + "/" + "insertloginhandler"
	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Name", name)
	req.Header.Add("Ip", ip)
	req.Header.Add("Port", port)

	if _, err = R.sendRequest(req); err != nil {
		return err
	}

	return nil
}

// InsertConnectionHandler inserts a single connection handler to rest server that expires
// in 10 seconds.
func (R *RESTClient) InsertConnectionHandler(name, ip, port string, userCount int) error {

	connString := "https://" + R.IP + ":" + R.Port + "/" + "insertconnectionhandler"
	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Name", name)
	req.Header.Add("Ip", ip)
	req.Header.Add("Port", port)
	req.Header.Add("Usercount", strconv.Itoa(userCount))

	if _, err = R.sendRequest(req); err != nil {
		return err
	}

	return nil
}

// InsertLoginToken inserts a single login token to rest server that expires in
// 1 seconds.
func (R *RESTClient) InsertLoginToken(userName, token string) error {
	connString := "https://" + R.IP + ":" + R.Port + "/" + "insertlogintoken"
	req, err := http.NewRequest("POST", connString, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Username", userName)
	req.Header.Add("Token", token)

	if _, err = R.sendRequest(req); err != nil {
		return err
	}

	return nil
}

// NewRestClient is a default constructor for RESTInfo.
func NewRestClient(IP, Port, Password string) (*RESTClient, error) {

	restClient := new(RESTClient)
	restClient.IP = IP
	restClient.Port = Port
	restClient.Secret = []byte(Password)

	var TLSConf tls.Config
	// skips check for Unsigned certs. REMOVE FOR PRODUCTION
	TLSConf.InsecureSkipVerify = true

	err := restClient.generateJWT()
	if err != nil {
		return nil, err
	}

	transport := &http.Transport{
		IdleConnTimeout: 200 * time.Millisecond,
	}
	transport.TLSClientConfig = &TLSConf
	restClient.HTTPClient = http.Client{Transport: transport}

	if err := testRestServerStatus(restClient.IP + ":" + restClient.Port); err != nil {
		return nil, err
	}

	return restClient, nil
}

func (R *RESTClient) sendRequest(req *http.Request) (string, error) {

	req.Header.Add("Jwttoken", R.JWTToken)

	resp, err := R.HTTPClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (R *RESTClient) generateJWT() error {

	token := jwt.New(jwt.SigningMethodHS256)

	tokenString, err := token.SignedString(R.Secret)
	if err != nil {
		return err
	}

	R.JWTToken = tokenString
	return nil
}

func testRestServerStatus(url string) error {

	_, err := http.Get("http://" + url)
	if err != nil {
		return err
	}
	return nil
}
