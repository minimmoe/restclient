# monitor-hook

thesis System monitor hook for parts of the system to register themselves to the monitor.

## Example

```go
func main() {

	Conf := confgetter.Config{}
	Conf.LoadFile("configuration.json")
	Conf.GetKeyValuePairs()

	hook := RESTHook{}
	if err := hook.Init(&Conf); err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	data := make(map[string]string)
	data["ip"] = "192..."
	data["port"] = "8888"
	data["last updated"] = "12.12.2024"

	jsongString, err := hook.GetJSON("")
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(jsongString)

	if err := hook.Hook("loginservers", "server 1", data); err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	jsongString2, err := hook.GetJSON("")
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(jsongString2)
}
```

## Monitor hook protocol

package consists of a json string and a package ID seperated by ";". Each packet ends with a new line character ("\n")

identifier: 0x68, 0x39, 0x7b, 0xb3

characters ; need to be sanitized to : before constructing the package as ; is reserved as seperator.

## Notes

- GetJSON function has implied root /
- Only works over TLS connection
- Currently cert InsecureSkipVerify is set to true which leaves the service open for man in the middle attacks (debug certs aren't signed)